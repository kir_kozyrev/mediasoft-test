$(document).ready(function(){

    let link = $('.m-home-menu-icon');
    let menu = $('.m-menu');
    let close = $('.close');
    let close_m = $('.close_m');
    let goTo = $('.goTo');

    link.on('click', function(event) {
        event.preventDefault();
        menu.toggleClass('m-menu-active');
    });

    close.on('click', function(event) {
        event.preventDefault();
        menu.toggleClass('m-menu-active');
    });

    close_m.on('click', function(event) {
        menu.toggleClass('m-menu-active');
    });

    goTo.on('click', function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 2000);
    });

    $(".regular").slick({
        dots: true,
        rows: 2,
        slidesPerRow: 2,
        infinite: true,
        arrows: false,
        responsive: [
            {
              breakpoint: 1090,
              settings: {
                rows: 1,
                slidesPerRow: 1,
                infinite: true,
                arrows: false
              }
            },
            {
                breakpoint: 1320,
                settings: {
                  rows: 1,
                  slidesPerRow: 2,
                  infinite: true,
                  arrows: false
                }
              }
          ]
    });

    let animationPlayed = false;
    $(".knob").knob();
    $(window).scroll(
        function() {
        var start = $(".work-box").offset().top-200;
        if ($(this).scrollTop() > start && !animationPlayed) {
            animationPlayed = true;
            $({animatedVal: 0}).animate({animatedVal: 90}, {
                duration: 2500,
                easing: "swing",
                step: function() {
                    $("#webDesign").val(Math.ceil(this.animatedVal)).trigger("change");
                }
            });
            $({animatedVal: 0}).animate({animatedVal: 75}, {
                duration: 2500,
                easing: "swing",
                step: function() {
                    $("#htmlCss").val(Math.ceil(this.animatedVal)).trigger("change");
                }
            });
            $({animatedVal: 0}).animate({animatedVal: 70}, {
                duration: 2500,
                easing: "swing",
                step: function() {
                    $("#graphicDesign").val(Math.ceil(this.animatedVal)).trigger("change");
                }
            });
            $({animatedVal: 0}).animate({animatedVal: 85}, {
                duration: 2500,
                easing: "swing",
                step: function() {
                    $("#uiUx").val(Math.ceil(this.animatedVal)).trigger("change");
                }
            });
        }
    });

    $('#kostyl').hide();
    $('#text_name').focus(function () {
        $('#label_text_name').hide();
        $('#kostyl').show();
    });
    $('#text_name').blur(function () {
        if ($(this).val().trim() === '') {
            $('#label_text_name').show();
            $('#kostyl').hide();
        }
    });

    $('#text_email').focus(function () {
        $('#label_text_email').hide();
    });
    $('#text_email').blur(function () {
        if ($(this).val().trim() === '') {
            $('#label_text_email').show();
        }
    });

    $('#kostyl2').hide();
    $('#text_message').focus(function () {
        $('#label_text_message').hide();
        $('#kostyl2').show();
    });
    $('#text_message').blur(function () {
        if ($(this).val().trim() === '') {
            $('#label_text_message').show();
            $('#kostyl2').hide();
        }
    });

    $('.rkn_denied').click(function(event) {
        event.preventDefault();
        $('.rkn-modal').show();
    });

    $('.close_rkn_modal').click(function() {
        $('.rkn-modal').hide();
    });

    $('#yesBtn').click(function() {
        $('.rkn-modal').hide();
        //location.href = 'https://ru.linkedin.com/';
        window.open('https://ru.linkedin.com/', '_blank');
    });
    $('#noBtn').click(function() {
        $('.rkn-modal').hide(1000);
    });
});