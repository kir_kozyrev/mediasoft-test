var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var minifyCSS = require('gulp-minify-css');
var csso = require('gulp-csso');

gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
    })
});

gulp.task('csso', function () {
    return gulp.src('app/css/**/*.css')
    .pipe(csso({
        restructure: false,
        sourceMap: true,
        debug: true
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('sass', function() {
    return gulp.src('app/scss/**/*.scss')
    .pipe(sass()) 
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
    stream: true
    }))
});


gulp.task('watch', ['browserSync', 'sass', ], function(){
    gulp.watch('app/scss/**/*.scss', ['sass']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload); 
});